# Wasserdichte Servo Durchführung

CAD Dokumente sind in diesem [Onshape Dokument](https://cad.onshape.com/documents/8b49ba9b6f0c3b1f108607a0/w/4f30e5eee1bbef8e3173e81a/e/b673eb2b7a3f85a40f00c276?renderMode=0&uiState=62446fb70535434fc3448df2) zu finden.

###### tags: `SearchWing`

# Wasserdichte Servo Durchführung

## Links 

[Messstifte für 15 € / stk.](https://www.messmittelonline.de/messstift16.html)

[Theorie zu Wellendichtringen](https://www.nilsson.co.at/fileadmin/user_upload/Kataloge/Nilsson_10919_2-DE-Wellendichtungen.pdf)

[Norm für Wellendichtringe](https://www-perinorm-com.ezproxy.hs-augsburg.de/document.aspx?hitnr=0&q=AC:DE18985034)

## Servo Motoren

[Spline (Verzahnung) Beschreibung](https://www.hepf.at/servo-spline-information/)

[Servo 25 Zähne Digital 37 € 11.5 g](https://hepf.com/GRAUPNER-DES-488-BB-MG-DIGITAL-SERVO)

[Serco 25 Zähne 14 € 12 g](https://hepf.com/KST-DS113MG-Digital-Servo-117mm-22kg)


## Servomotor Wellenadapter

[Adapter](https://www.servocity.com/servo-rotary-driver/) kann  C24T / H25T Servo Anschlüsse direkt auf 1 - 6 mm Starke welle übertragen.

[alternativer Verkäufer](https://www.active-robots.com/servo-rotary-driver.html)

![](https://i.imgur.com/WK6vadz.png)

Gibt es auch als [Metall](https://www.servocity.com/servo-to-shaft-coupler-h25t-spline-1-4-bore/), dann für 1/4 Inch (~ 6.35 mm).


## Wellendichtring 12 mm

Test-Lösung für diesen [Wellendichtring](https://www.diehr-rabenstein.de/shop/de/radial-wellendichtring-4-x-12-x-6-mm-da-nbr-70.html).

Wellendichtring für 4 mm Welle, benötigt 12 mm Gehäusebohrung mit 6,3 mm Tiefe.

GFK Platte mit 5 mm Stärke vorhanden, kann zum teseten gestapelt und zusammengeklebt werden -> 10 mm Stark. Alternativ 10 mm Starke Platte bestellen.

Konstruktion hat bereits sehr große Bauteile ergeben die ein hohes Gewicht für die Drohne bedeuten würde. Deswegen wurde dieser Wellendichtring nicht weiter verfolgt

## Wellendichtring 8 mm

[Wellendichtring](https://www.bauer-modelle.com/epages/Bauer_Uwe46269592.sf/de_DE/?ObjectPath=/Shops/Bauer_Uwe46269592/Products/4.54482) mit 8 mm Außendurchmesser, hat eine Stärke von nur 2 mm.

Aufbau benötigt ein [Gleitlager](https://www.conrad.de/de/p/igus-jfm-0405-06-gleitlager-bohrungs-4-mm-1416602.html?hk=SEM&WT.srch=1&WT.mc_id=google_pla&s_kwcid=AL%21222%213%21547715743126%21%21%21u%21%21&ef_id=CjwKCAjwiuuRBhBvEiwAFXKaNJFh_7ufgN-rMCvIriJp4N3ZunteHZAdWqEVY1uBtlLcfvEmXUkSMRoC4aIQAvD_BwE%3AG%3As&gclid=CjwKCAjwiuuRBhBvEiwAFXKaNJFh_7ufgN-rMCvIriJp4N3ZunteHZAdWqEVY1uBtlLcfvEmXUkSMRoC4aIQAvD_BwE#productTechData).


### Testaufbau

Diese Scheibe wurde für einen Testaufbau aus einer 5 mm starken GFK Platte gefertigt:

![](https://i.imgur.com/8ZvhIVL.png)

### Testergebnisse

Es wurden 2 Tests durchgeführt:

Zuerst wurde der Testzylinder unter Wasser gehalten und 1 Bar Luftdruck auf den Innenraum gegeben. Dabei ist eine Luftblase alle 50 - 60 Sekunden an der 4 mm starken welle ausgetreten.

Im nächstem Versuch wurde ca. 2 cm Wassertiefe in den Zylinder gegeben und 1 Bar Luftdruch auf den Innenraum gegeben. Das zeigt den tatsächlichen anwendungsfall bei 10 m Wassersäule. Wir wollen auf 1 m auslegen. Nach 20 minuten ist kein Wasser ausgetreten.

![](https://i.imgur.com/YvyDs3c.jpg)

![](https://i.imgur.com/0pDvUFU.png)


Dieser Versuch war sehr vielversprechend und zeigt eine mögliche Anwendungsweise.

## O-Ring 

Desweiteren wird ieser [O-Ring](https://www.diehr-rabenstein.de/shop/de/dichtring-o-ring-4-x-1-5-mm-nbr-70.html) getestet.

Für Nut-Dimensionen wird diese [Tabelle](https://www.cog.de/produkte/rund-um-den-o-ring/o-ring-einbaumasse/tabellarisch) herangezogen. Daraus ergeben sich für diese Grafik folgende werte:
![](https://i.imgur.com/XlcgbYB.png)

    t = 1.15 mm
    b = 2.19 mm

Aus diesen Werten wurde eine Dichtscheibe für den bestehenden Testaufbau von Jonathan Zeidler konstruiert:

![O-Ring-Dichtscheibe.png](./O-Ring-Dichtscheibe.png)

Nachdem bereits eine Testscheibe für den Wellendichtring hergestellt wurde, wird keine weitere gefräst, sondern die benötigte Bohrung in die bereits existierende eingefräst.













