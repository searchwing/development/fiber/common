# Fiber Pipe Fuselage

The fiber pipe fuselage is build with 50mm fiber pipes. 

faser-cog.ods contains a center of gravity calculation

faser-rohr.xfl contains an xflr5 model of the fiber plane. 

## Light Fiber COG and Wing position

The "light" fiber plane is with the SLS26004120 battery and without the camera system.

  * Weight 1870g
  * COG 55cm from tip of fuselage
  * Main wings leading edge at 47.5cm from tip of fuselage
  * VTail angle 35° (=110° between Vtails), chord 17cm, length 31cm, 26% = 4cm rudder length

